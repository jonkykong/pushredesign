//
//  pushTalkManyVC.m
//  Push
//
//  Created by Jon Kent on 2/11/15.
//  Copyright (c) 2015 Jon Kent. All rights reserved.
//

#import "pushTalkManyVC.h"
#import "pushImageCell.h"
#import "pushFriendOptionsVC.h"

@interface pushTalkManyVC ()

@property (nonatomic, weak) IBOutlet UIButton *pushToTalkButton;
@property (nonatomic, weak) IBOutlet UILabel *friendCountLabel;
@property (nonatomic, strong) IBOutlet UIView *tableHeaderView;

@end

@implementation pushTalkManyVC

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.friendCountLabel.text = [NSString stringWithFormat:@"%lu friends on this PUSH:", (unsigned long)self.recipients.count];
    self.friendCountLabel.textColor = self.view.tintColor;
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    UIViewController *viewController = [segue destinationViewController];
    if([viewController isKindOfClass:[pushFriendOptionsVC class]])
    {
        pushFriendOptionsVC *pushOptionsVC = (id)viewController;
        NSIndexPath *indexPath = [self.tableView indexPathForCell:sender];
        pushOptionsVC.contact = self.recipients[indexPath.row];
    }
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    // Return the number of rows in the section.
    return self.recipients.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    pushImageCell *cell = [tableView dequeueReusableCellWithIdentifier:
                           [pushImageCell cellIdentifierForImageCount:1
                                                          hasSubtitle:YES]
                                                          forIndexPath:indexPath];
    
    cell.tintColor = self.view.tintColor;
    
    // TODO: Configure the cell...
    UIImage *image = [UIImage imageNamed:@"headshot.jpg"];
    
    [cell setTitle:@"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas non malesuada tellus. Nam et massa ac dui porta molestie sit amet ac neque." subTitle:@"@username" images:@[image]];
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return [pushImageCell cellHeight];
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    self.tableView.tableHeaderView = nil;
    return self.tableHeaderView;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return self.tableHeaderView.frame.size.height;
}

@end
