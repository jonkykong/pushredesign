//
//  pushGroupThreadVC.m
//  Push
//
//  Created by Jon Kent on 2/12/15.
//  Copyright (c) 2015 Jon Kent. All rights reserved.
//

#import "pushGroupThreadVC.h"
#import "pushImageCell.h"

@interface pushGroupThreadVC ()

@property (nonatomic, strong) NSArray *contacts;
@property (nonatomic, strong) IBOutlet UIView *doneView;
@property (nonatomic, weak) IBOutlet UIButton *doneButton;
@property (nonatomic, weak) IBOutlet UILabel *promptLabel;
@property (nonatomic, weak) IBOutlet UIImageView *pushLogo;

@end

@implementation pushGroupThreadVC

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    // TODO: populate arrays with real data
    self.contacts = [NSMutableArray arrayWithObjects:@"1", @"2", @"3", @"4", @"5", @"6", @"7", @"8", @"9", @"10", nil];
    
    [self.doneButton setTitle:self.buttonTitle forState:UIControlStateNormal];
    self.promptLabel.text = self.viewSubtitle;
    
    self.promptLabel.textColor = self.view.tintColor;
    
    self.tableView.tableFooterView = nil;
    [self.parentViewController.view addSubview:self.doneView];
    
    CGRect frame = self.doneView.frame;
    frame.size.height = 60;
    frame.origin.y = self.parentViewController.view.frame.size.height - frame.size.height;
    self.doneView.frame = frame;
    [self.tableView setContentInset:UIEdgeInsetsMake(self.tableView.contentInset.top,
                                                     self.tableView.contentInset.left,
                                                     frame.size.height,
                                                     self.tableView.contentInset.right)];
    
    self.doneButton.layer.cornerRadius = 3;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    if(self.navigationController.navigationBar.barStyle == UIBarStyleBlack)
    {
        self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
        self.doneView.backgroundColor = [UIColor clearColor];
        self.doneButton.backgroundColor = [UIColor colorWithRed:0.15 green:0.15 blue:0.15 alpha:0.95];
        self.doneButton.layer.borderColor = [UIColor whiteColor].CGColor;
        self.doneButton.layer.borderWidth = 1;
        self.promptLabel.textColor = [UIColor whiteColor];
        self.pushLogo.image = [UIImage imageNamed:@"smallPUSHwhite.png"];
    }
    else
    {
        self.doneButton.backgroundColor = self.view.tintColor;
    }
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    // Return the number of rows in the section.
    return self.contacts.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    pushImageCell *cell;
    if(indexPath.section == 0)
    {
        cell = [tableView dequeueReusableCellWithIdentifier:
                [pushImageCell cellIdentifierForImageCount:1
                                               hasSubtitle:YES]
                                               forIndexPath:indexPath];
    }
    else
    {
        cell = [tableView dequeueReusableCellWithIdentifier:
                [pushImageCell cellIdentifierForImageCount:1
                                               hasSubtitle:NO]
                                               forIndexPath:indexPath];
    }
    
    if(self.navigationController.navigationBar.barStyle == UIBarStyleBlack)
    {
        cell.backgroundColor = [UIColor clearColor];
        cell.titleLabel.textColor = [UIColor whiteColor];
        cell.subtitleLabel.textColor = [UIColor whiteColor];
    }
    cell.tintColor = self.view.tintColor;
    
    // TODO: Configure the cell...
    if(indexPath.section == 0)
    {
        UIImage *image = [UIImage imageNamed:@"headshot.jpg"];
        
        [cell setTitle:@"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas non malesuada tellus. Nam et massa ac dui porta molestie sit amet ac neque." subTitle:@"@username" images:@[image]];
    }
    else
    {
        // don't pass an image to instead use checkmarks for selected items
        [cell setTitle:@"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas non malesuada tellus. Nam et massa ac dui porta molestie sit amet ac neque." subTitle:@"@username" images:nil];
    }
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return [pushImageCell cellHeight];
}

#pragma mark - Navigation

- (IBAction)close:(id)sender
{
    [self.delegate pushGroupThreadVCDidCancel:self];
    
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)sendInvites:(id)sender
{
    NSArray *indexPaths = [self.tableView indexPathsForSelectedRows];
    
    NSMutableArray *inviteArray = [NSMutableArray arrayWithCapacity:indexPaths.count];
    for(NSIndexPath *indexPath in indexPaths)
    {
        [inviteArray addObject:self.contacts[indexPath.row]];
    }
    
    [self.delegate pushGroupThreadVC:self didFinishPickingFriends:inviteArray];
    
    [self dismissViewControllerAnimated:YES completion:nil];
}

@end
