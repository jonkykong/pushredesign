//
//  pushVextVC.m
//  Push
//
//  Created by Jon Kent on 2/10/15.
//  Copyright (c) 2015 Jon Kent. All rights reserved.
//

#import "pushVextsVC.h"
#import "pushImageCell.h"
#import "pushVextVC.h"

@interface pushVextsVC ()

@property (nonatomic, strong) NSArray *vextThreads;

@end

@implementation pushVextsVC

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.tableView.backgroundView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"vextBack.jpg"]];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    // TODO: populate arrays with real data
    self.vextThreads = [NSMutableArray arrayWithObjects:@"1", @"2", @"3", @"4", @"5", @"6", @"7", @"8", @"9", @"10", nil];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    UIViewController *viewController = [segue destinationViewController];
    if([viewController isKindOfClass:[pushVextVC class]])
    {
        pushVextVC *pushVextVC = (id)viewController;
        pushVextVC.recipients = @[@"1", @"2", @"3", @"4", @"5", @"6", @"7", @"8", @"9", @"10"];
    }
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    // Return the number of rows in the section.
    return self.vextThreads.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    pushImageCell *cell = [tableView dequeueReusableCellWithIdentifier:
                           [pushImageCell cellIdentifierForImageCount:indexPath.row
                                                          hasSubtitle:(indexPath.row == 0)]
                                                          forIndexPath:indexPath];
    
    cell.tintColor = self.view.tintColor;
    cell.backgroundColor = [UIColor clearColor];
    cell.titleLabel.textColor = [UIColor whiteColor];
    cell.subtitleLabel.textColor = [UIColor whiteColor];
    
    // TODO: Configure the cell...
    NSMutableArray *imageArray = [NSMutableArray array];
    UIImage *image = [UIImage imageNamed:@"headshot.jpg"];
    for(int i = 0; i < indexPath.row; i++)
    {
        [imageArray addObject:image];
    }
    
    if(imageArray.count == 0)
    {
        [imageArray addObject:image];
    }
    
    [cell setTitle:@"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas non malesuada tellus. Nam et massa ac dui porta molestie sit amet ac neque." subTitle:@"@username" images:imageArray];
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return [pushImageCell cellHeight];
}

@end
