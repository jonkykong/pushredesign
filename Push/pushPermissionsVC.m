//
//  pushPermissionsVC.m
//  Push
//
//  Created by Jon Kent on 2/7/15.
//  Copyright (c) 2015 Jon Kent. All rights reserved.
//

#import "pushPermissionsVC.h"
#import <AVFoundation/AVFoundation.h>
#import <AddressBook/AddressBook.h>
#import "pushAppDelegate.h"

@interface pushPermissionsVC () <UIAlertViewDelegate>

@property (nonatomic, weak) IBOutlet UILabel *permissionTitleLabel;
@property (nonatomic, weak) IBOutlet UISwitch *permissionSwitch;
@property (nonatomic, weak) IBOutlet UILabel *permissionDescriptionLabel;
@property (nonatomic, weak) IBOutlet UIImageView *permissionImageView;
@property (nonatomic, weak) IBOutlet UIView *contentView;
@property (nonatomic, weak) IBOutlet UILabel *countLabel;
@property (nonatomic, weak) UIImageView *handImageView;

@end

@implementation pushPermissionsVC

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    NSArray *permissionImages = @[@"permissionMic.png",
                                  @"permissionContacts.png",
                                  @"permissionPushNotif.png"];
    NSArray *permissionTitles = @[@"Microphone",
                                  @"Contacts",
                                  @"Push Notifications"];
    NSArray *permissionDescriptions = @[@"Turn on the microphone so your friends can hear you when you PUSH them.",
                                        @"The fastest and easiest way to get started is to access your contacts & see who's already on PUSH.",
                                        @"Turn on push notifications to know when your friends are PUSHing you."];
    NSArray *countMessages = @[@"1 of 3",
                               @"2 of 3",
                               @"3 of 3"];
    self.permissionTitleLabel.text = permissionTitles[self.permissionType];
    self.permissionDescriptionLabel.text = [permissionDescriptions[self.permissionType] stringByAppendingString:@"\n\n"]; // add two newlines so text doesn't get centered vertically in a 3 line height label
    self.permissionImageView.image = [UIImage imageNamed:permissionImages[self.permissionType]];
    self.countLabel.text = countMessages[self.permissionType];
    
    if(self.permissionType == PushPermissionsTypeNotifications)
    {
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(receivedPushNotificationRegistrationResponse) name:PushRegisteredForPushNotifications object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(nextPage) name:PushFailedRegisterForPushNotifications object:nil];
    }
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    self.permissionSwitch.on = NO;
    [self.handImageView removeFromSuperview];
    
    [self.navigationController.navigationBar setBackgroundImage:nil forBarMetrics:UIBarMetricsDefault];

    // Hack to fix Xcode table cell sizing issues: http://stackoverflow.com/questions/26230247/auto-resizing-issue-in-uitableview-in-xcode-6
    [self.permissionTitleLabel sizeToFit];
    CGRect frame = self.permissionSwitch.frame;
    frame.origin.x = [UIScreen mainScreen].bounds.size.width - self.permissionSwitch.frame.size.width - 8;
    self.permissionSwitch.frame = frame;
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    // Hack to fix Xcode table cell sizing issues: http://stackoverflow.com/questions/26230247/auto-resizing-issue-in-uitableview-in-xcode-6
    self.permissionSwitch.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin;
}

#pragma mark - Permissions

-(void) handleMic: (id)sender {
    if([[AVAudioSession sharedInstance] respondsToSelector:@selector(requestRecordPermission:)]){
        [[AVAudioSession sharedInstance] requestRecordPermission:^(BOOL granted) {
            dispatch_async(dispatch_get_main_queue(), ^{
                if (granted) {
                    NSLog(@"pushPermissionsController::handleMic: GRANTED");
                    [self nextPage];
                } else {
                    NSLog(@"pushPermissionsController::handleMic: DENIED");
                    [self.permissionSwitch setOn:NO animated:YES];
                    [self showSettingsPromptWithTitle:@"Microphone Access Denied"
                                              message:@"You must allow microphone access in Settings->Privacy->Microphone"];
                }
            });
        }];
    }else{
        NSLog(@"pushPermissionsController::handleMic: ERROR mic not detected alert the user");
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.permissionSwitch setOn:NO animated:YES];
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Microphone Not Found"
                                                            message:@"Your device doesn't seem to have a mic. Sorry this app doesn't function without a microphone."
                                                           delegate:nil
                                                  cancelButtonTitle:@"OK"
                                                  otherButtonTitles:nil];
            [alert show];
        });
    }
}

-(void) handleAPNS: (id)sender
{
    if ([[UIApplication sharedApplication] respondsToSelector:@selector(registerUserNotificationSettings:)]){
        NSLog(@"pushPermissionsController::handleAPNS: ios8 path");
        [[UIApplication sharedApplication] registerUserNotificationSettings:[UIUserNotificationSettings settingsForTypes:(UIUserNotificationTypeSound | UIUserNotificationTypeAlert | UIUserNotificationTypeBadge) categories:nil]];
        [[UIApplication sharedApplication] registerForRemoteNotifications];
    }else{
        NSLog(@"enabledRemoteNotificationTypes 2");
        [[UIApplication sharedApplication] registerForRemoteNotificationTypes:
         (UIUserNotificationTypeBadge | UIUserNotificationTypeSound | UIUserNotificationTypeAlert)];
    }
    
    [self performSelector:@selector(delayedNextPageIfNecessary) withObject:nil afterDelay:0.7];
}

- (void)delayedNextPageIfNecessary
{
    if([UIApplication sharedApplication].applicationState == UIApplicationStateActive)
    {
        [self nextPage];
    }
    else
    {
        [self showHandForPushNotificationsPrompt:YES];
    }
}

-(void) handleAddressBook: (id)sender
{
    ABAddressBookRef addressBookRef = ABAddressBookCreateWithOptions(NULL, NULL);
    ABAddressBookRequestAccessWithCompletion(addressBookRef, ^(bool granted, CFErrorRef error) {
        dispatch_async(dispatch_get_main_queue(), ^{
            if (granted) {
                NSLog(@"pushPermissionsController::handleAdressBook: granted");
                [self nextPage];
            } else {
                NSLog(@"pushPermissionsController::handleAdressBook: DENIED");
                [self.permissionSwitch setOn:NO animated:YES];
                [self showSettingsPromptWithTitle:@"Address Book Access Denied"
                                          message:@"To allow access to the address book goto Settings->Privacy->Contacts and enable Push"];
            }
        });
    });
}

- (void)showSettingsPromptWithTitle:(NSString *)title message:(NSString *)message
{
    static UIAlertView *authorizationAlertView;
    
    if([authorizationAlertView isVisible]) return;
    
    if(!authorizationAlertView)
    {
        if(&UIApplicationOpenSettingsURLString != NULL)
        {
            authorizationAlertView = [[UIAlertView alloc] initWithTitle:nil
                                                                message:nil
                                                               delegate:self
                                                      cancelButtonTitle:@"Close"
                                                      otherButtonTitles:@"Settings", nil];
        }
        else
        {
            authorizationAlertView = [[UIAlertView alloc] initWithTitle:nil
                                                                message:nil
                                                               delegate:nil
                                                      cancelButtonTitle:@"OK"
                                                      otherButtonTitles:nil];
        }
    }
    
    authorizationAlertView.title = title;
    authorizationAlertView.message = message;
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [authorizationAlertView show];
    });
}

- (void)showHandForPushNotificationsPrompt:(BOOL)pushNotifications
{
    dispatch_async(dispatch_get_main_queue(), ^{
        UIImageView *imageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"pointerHand.png"]];
        imageView.alpha = 0;
        [self.tableView addSubview:imageView];
        CGRect frame = imageView.frame;
        if(pushNotifications)
        {
            frame.origin.y = [UIScreen mainScreen].bounds.size.height * 0.55;
        }
        else
        {
            frame.origin.y = [UIScreen mainScreen].bounds.size.height * 0.5;
        }
        frame.origin.x = [UIScreen mainScreen].bounds.size.width * 0.6;
        imageView.frame = frame;
        self.handImageView = imageView;
        [UIView animateWithDuration:0.35 animations:^{
            imageView.alpha = 1.0;
        }];
    });
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if([[alertView buttonTitleAtIndex:buttonIndex] isEqualToString:@"Settings"])
    {
        NSURL *appSettings = [NSURL URLWithString:UIApplicationOpenSettingsURLString];
        [[UIApplication sharedApplication] openURL:appSettings];
    }
    else if([[alertView buttonTitleAtIndex:buttonIndex] isEqualToString:@"Skip"])
    {
        [self nextPage];
    }
}

#pragma mark - Navigation

- (void)nextPage
{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    
    UIViewController *nextPage;
    
    if(self.permissionType == PushPermissionsTypeNotifications)
    {
        nextPage = [storyboard instantiateViewControllerWithIdentifier:@"MainVC"];
    }
    else
    {
        nextPage = [storyboard instantiateViewControllerWithIdentifier:@"PushPermissionsVC"];
        pushPermissionsVC *pushVC = (id)nextPage;
        pushVC.permissionType = self.permissionType + 1;
    }
    
    if (!nextPage.view.window)
    {
        dispatch_async(dispatch_get_main_queue(), ^{
            
            if(self.permissionType == PushPermissionsTypeNotifications)
            {
                [[NSNotificationCenter defaultCenter] removeObserver:self];
                [self.navigationController setViewControllers:@[nextPage] animated:YES];
            }
            else
            {
                [self.navigationController pushViewController:nextPage animated:YES];
            }
        });
    }
}

- (void)receivedPushNotificationRegistrationResponse
{
    [self nextPage];
}

- (IBAction)switchChange:(id)sender
{
    if(self.permissionType == PushPermissionsTypeMicrophone)
    {
        [self performSelector:@selector(showHandForPushNotificationsPrompt:) withObject:[NSNumber numberWithBool:NO] afterDelay:0.7];
        [self performSelector:@selector(handleMic:) withObject:nil afterDelay:0.35];
    }
    else if(self.permissionType == PushPermissionsTypeContacts)
    {
        [self performSelector:@selector(showHandForPushNotificationsPrompt:) withObject:[NSNumber numberWithBool:NO] afterDelay:0.7];
        [self performSelector:@selector(handleAddressBook:) withObject:nil afterDelay:0.35];
    }
    else
    {
        [self performSelector:@selector(handleAPNS:) withObject:nil afterDelay:0.35];
    }
}

@end
