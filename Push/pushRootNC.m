//
//  pushRootNC.m
//  Push
//
//  Created by Jon Kent on 2/6/15.
//  Copyright (c) 2015 Jon Kent. All rights reserved.
//

#import "pushRootNC.h"

@implementation pushRootNC

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    UIColor *globalTint = self.navigationBar.tintColor;
    
    [[UITabBarItem appearance] setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIColor darkGrayColor], NSForegroundColorAttributeName, nil] forState:UIControlStateNormal];
    [[UITabBarItem appearance] setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:globalTint, NSForegroundColorAttributeName, nil] forState:UIControlStateSelected];
    [[UITabBar appearance] setTintColor:globalTint];
    
    BOOL isSignedIn = NO; // define logic here for when a user is signed in or not
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    
    UIViewController *nextPage;
    
    if(!isSignedIn)
    {
        nextPage = [storyboard instantiateViewControllerWithIdentifier:@"FirstRunVC"];
    }
    else
    {
        nextPage = [storyboard instantiateViewControllerWithIdentifier:@"MainVC"];
    }
    
    if (!nextPage.view.window)
    {
        [self pushViewController:nextPage animated:NO];
    }
}

@end
