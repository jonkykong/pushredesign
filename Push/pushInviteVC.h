//
//  pushInviteVC.h
//  Push
//
//  Created by Jon Kent on 2/12/15.
//  Copyright (c) 2015 Jon Kent. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface pushContactSection : NSObject

@property (nonatomic, strong) NSArray *contacts;
@property (nonatomic, strong) NSString *sectionTitle;

@end

@interface pushInviteVC : UITableViewController

@end
