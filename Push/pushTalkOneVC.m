//
//  pushTalkVC.m
//  Push
//
//  Created by Jon Kent on 2/11/15.
//  Copyright (c) 2015 Jon Kent. All rights reserved.
//

#import "pushTalkOneVC.h"

@interface pushTalkOneVC ()

@property (nonatomic, weak) IBOutlet UIImageView *friendImageView;
@property (nonatomic, weak) IBOutlet UILabel *nameLabel;
@property (nonatomic, weak) IBOutlet UILabel *usernameLabel;
@property (nonatomic, weak) IBOutlet UIButton *pushToTalkButton;

@end

@implementation pushTalkOneVC

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    CGRect frame = self.friendImageView.frame;
    frame.size.width = frame.size.height;
    frame.origin.x = ((self.view.frame.size.width - frame.size.width) / 2.0);
    self.friendImageView.frame = frame;
    self.friendImageView.layer.cornerRadius = self.friendImageView.frame.size.height / 2.0;
    self.friendImageView.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin |
                                            UIViewAutoresizingFlexibleRightMargin |
                                            UIViewAutoresizingFlexibleTopMargin;
}



@end
