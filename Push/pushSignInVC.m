//
//  pushSignInVC.m
//  Push
//
//  Created by Jon Kent on 2/6/15.
//  Copyright (c) 2015 Jon Kent. All rights reserved.
//

#import "pushSignInVC.h"

@interface pushSignInVC ()

@property (nonatomic, weak) IBOutlet UIButton *logInButton;

@end

@implementation pushSignInVC

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self.navigationController setNavigationBarHidden:NO animated:YES];
    [self.navigationController.navigationBar setBackgroundImage:[UIImage new] forBarMetrics:UIBarMetricsDefault];
    [self.navigationController.navigationBar setShadowImage:[UIImage new]];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];

}

- (void)keyboardWillShow:(NSNotification*)notification
{
    NSValue* bv = notification.userInfo[UIKeyboardFrameEndUserInfoKey];
    CGRect br = [bv CGRectValue];
    
    NSValue* ad = notification.userInfo[UIKeyboardAnimationDurationUserInfoKey];
    NSTimeInterval animationDuration = 0;
    [ad getValue:&animationDuration];
    
    NSNumber *ao = notification.userInfo[UIKeyboardAnimationCurveUserInfoKey];
    NSUInteger animationOptions;
    [ao getValue:&animationOptions];
    
    CGRect frame = self.view.frame;
    frame.size.height -= br.size.height;
    
    [UIView animateWithDuration:animationDuration delay:0 options:animationOptions
                     animations:^{
                             self.view.frame = frame;
                     }
                     completion:nil];
}

- (void)keyboardWillHide:(NSNotification*)notification
{
    NSValue* ad = notification.userInfo[UIKeyboardAnimationDurationUserInfoKey];
    NSTimeInterval animationDuration = 0;
    [ad getValue:&animationDuration];
    
    NSNumber *ao = notification.userInfo[UIKeyboardAnimationCurveUserInfoKey];
    NSUInteger animationOptions;
    [ao getValue:&animationOptions];
    
    [UIView animateWithDuration:animationDuration delay:0 options:animationOptions
                     animations:^{
                         self.view.frame = [[UIScreen mainScreen] bounds];
                     }
                     completion:nil];
}

- (BOOL)shouldPerformSegueWithIdentifier:(NSString *)identifier sender:(id)sender
{
    // TODO: perform sign in here, if sign in succeeds, return YES, otherwise, return NO
    
    return YES;
}

@end
