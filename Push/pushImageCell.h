//
//  pushContactCell.h
//  Push
//
//  Created by Jon Kent on 2/9/15.
//  Copyright (c) 2015 Jon Kent. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface pushImageCell : UITableViewCell

@property (nonatomic, weak) IBOutlet UILabel *titleLabel;
@property (nonatomic, weak) IBOutlet UILabel *subtitleLabel;

- (void)setTitle:(NSString *)title subTitle:(NSString *)subTitle images:(NSArray *)imageArray;
+ (NSString *)cellIdentifierForImageCount:(NSUInteger)count hasSubtitle:(BOOL)subtitle;
+ (CGFloat)cellHeight;

@end
