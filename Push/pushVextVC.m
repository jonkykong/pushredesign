//
//  pushVextVC.m
//  Push
//
//  Created by Jon Kent on 2/11/15.
//  Copyright (c) 2015 Jon Kent. All rights reserved.
//

#import "pushVextVC.h"

@interface pushVextVC () <UITableViewDataSource, UITableViewDelegate, UITextFieldDelegate>

@property (nonatomic, weak) IBOutlet UITextField *vextTextField;
@property (nonatomic, weak) IBOutlet UILabel *waitingLabel;
@property (nonatomic, weak) IBOutlet UITableView *tableView;
@property (nonatomic, strong) NSMutableArray *vexts;

@end

@implementation pushVextVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.vexts = [NSMutableArray array];
    
    self.vextTextField.delegate = self;
    CGFloat red, green, blue;
    [self.view.tintColor getRed:&red green:&green blue:&blue alpha:nil];
    UIColor *tintColorAlpha = [UIColor colorWithRed:red green:green blue:blue alpha:0.5];
    self.waitingLabel.backgroundColor = tintColorAlpha;
    [self.vextTextField becomeFirstResponder];
    self.tableView.transform = CGAffineTransformMakeScale(1, -1);
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    [self.view endEditing:YES];
}

#pragma mark - Navigation

- (IBAction)close:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - Table View

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"VextCell" forIndexPath:indexPath];
    
    cell.textLabel.text = self.vexts[indexPath.row];
    
    cell.textLabel.layer.shadowColor = [[UIColor blackColor] CGColor];
    cell.textLabel.layer.shadowOffset = CGSizeMake(0.0f, 0.0f);
    cell.textLabel.layer.shadowOpacity = 1.0f;
    cell.textLabel.layer.shadowRadius = 1.0f;
    
    cell.transform = CGAffineTransformMakeScale(1, -1);
    
    return cell;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.vexts.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 44.0;
}

# pragma mark - textField Delegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if(textField.text.length == 0) return NO;
    
    [self addVext:textField.text];
    textField.text = nil;
    
    return YES;
}

- (void)addVext:(NSString *)vext
{
    [self.vexts insertObject:vext atIndex:0];
    [self.tableView insertRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:0 inSection:0]] withRowAnimation:UITableViewRowAnimationBottom];

    NSUInteger visibleCellCount = self.tableView.frame.size.height / 44.0 - 1;
    
    if(self.vexts.count > visibleCellCount)
    {
        [self.vexts removeLastObject];
        [self.tableView deleteRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:visibleCellCount inSection:0]]  withRowAnimation:UITableViewRowAnimationFade];
    }

    NSUInteger index = visibleCellCount;
    for(UITableViewCell *cell in self.tableView.visibleCells)
    {
        [UIView animateWithDuration:0.35 animations:^{
            cell.textLabel.alpha = index / (CGFloat)visibleCellCount;
        }];
        index--;
    }
}

@end
