//
//  pushFriendOptionsVC.h
//  Push
//
//  Created by Jon Kent on 2/10/15.
//  Copyright (c) 2015 Jon Kent. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface pushFriendOptionsVC : UITableViewController

@property (nonatomic, strong) id contact;

@end
