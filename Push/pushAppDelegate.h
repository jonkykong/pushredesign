//
//  AppDelegate.h
//  Push
//
//  Created by Jon Kent on 2/3/15.
//  Copyright (c) 2015 Jon Kent. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface pushAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

UIKIT_EXTERN NSString *const PushRegisteredForPushNotifications;
UIKIT_EXTERN NSString *const PushFailedRegisterForPushNotifications;

@end

