//
//  pushAccountInfoVC.m
//  Push
//
//  Created by Jon Kent on 2/6/15.
//  Copyright (c) 2015 Jon Kent. All rights reserved.
//

#import "pushAccountInfoVC.h"
#import "pushRootNC.h"

@interface pushAccountInfoVC () <UITextViewDelegate>

@property (nonatomic, weak) IBOutlet UIButton *setPhotoButton;
@property (nonatomic, weak) IBOutlet UITextField *firstNameTextField;
@property (nonatomic, weak) IBOutlet UITextField *lastNameTextField;
@property (nonatomic, weak) IBOutlet UITextField *usernameTextField;
@property (nonatomic, weak) IBOutlet UITextField *emailTextField;
@property (nonatomic, weak) IBOutlet UITextField *passwordTextField;
@property (nonatomic, weak) IBOutlet UILabel *disclaimerTextView;

@end

@implementation pushAccountInfoVC

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.setPhotoButton.clipsToBounds = YES;
    self.setPhotoButton.layer.cornerRadius = self.setPhotoButton.frame.size.width / 2.0;
    self.setPhotoButton.layer.borderWidth = 0.5;
    self.setPhotoButton.layer.borderColor = self.firstNameTextField.textColor.CGColor;
    
    UIFont *regularFont = [UIFont fontWithName:@"PT Sans" size:16];
    NSMutableParagraphStyle *paragraph =[[NSMutableParagraphStyle alloc] init];
    [paragraph setAlignment:NSTextAlignmentCenter];
    NSDictionary *regularAttrs = @{NSFontAttributeName : regularFont,
                                   NSParagraphStyleAttributeName : paragraph,
                                   NSForegroundColorAttributeName : [UIColor colorWithRed:109/255.0 green:109/255.0 blue:115/255.0 alpha:1]};
    
    NSMutableDictionary *linkAttrs = [NSMutableDictionary dictionaryWithDictionary:regularAttrs];
    [linkAttrs addEntriesFromDictionary:@{NSForegroundColorAttributeName : self.view.tintColor}];;
    
    NSMutableAttributedString *attributedText = [[NSMutableAttributedString alloc] initWithString:self.disclaimerTextView.text attributes:regularAttrs];
    
    for(NSString *linkString in @[@"Terms of Service", @"Privacy Policy"])
    {
        NSRange range = [self.disclaimerTextView.text rangeOfString:linkString];
        [attributedText setAttributes:linkAttrs range:range];
    }
    
    self.disclaimerTextView.attributedText = attributedText;
    
    // if editing an existing account:
    if(![self.navigationController isKindOfClass:[pushRootNC class]])
    {
        self.title = @"My Account";
        
        // TODO: turn off any account fields that shouldn't be mucked with
        [self.usernameTextField setEnabled:NO];
        [self.passwordTextField setEnabled:NO];
        
        [self.navigationItem setRightBarButtonItem:[[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemSave target:self action:@selector(saveAndClose:)]];
        
        self.tableView.tableFooterView = nil;
    }
    else
    {
        // new account, so no cancel button
        [self.navigationItem setLeftBarButtonItem:nil];
    }
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self.navigationController setNavigationBarHidden:NO animated:YES];
    
    // Hack to fix Xcode table cell sizing issues: http://stackoverflow.com/questions/26230247/auto-resizing-issue-in-uitableview-in-xcode-6
    CGRect frame = self.firstNameTextField.frame;
    frame.size.width = [UIScreen mainScreen].bounds.size.width - self.firstNameTextField.frame.origin.x - 8;
    self.firstNameTextField.frame = frame;
    
    frame = self.lastNameTextField.frame;
    frame.size.width = [UIScreen mainScreen].bounds.size.width - self.lastNameTextField.frame.origin.x - 8;
    self.lastNameTextField.frame = frame;
    
    frame = self.usernameTextField.frame;
    frame.size.width = [UIScreen mainScreen].bounds.size.width - self.usernameTextField.frame.origin.x - 8;
    self.usernameTextField.frame = frame;
    
    frame = self.emailTextField.frame;
    frame.size.width = [UIScreen mainScreen].bounds.size.width - self.emailTextField.frame.origin.x - 8;
    self.emailTextField.frame = frame;
    
    frame = self.passwordTextField.frame;
    frame.size.width = [UIScreen mainScreen].bounds.size.width - self.passwordTextField.frame.origin.x - 8;
    self.passwordTextField.frame = frame;
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    [self.view endEditing:YES];
}

- (BOOL)shouldPerformSegueWithIdentifier:(NSString *)identifier sender:(id)sender
{
    return [self createAccount];
}

#pragma mark - Account Validation

- (BOOL)validateInput
{
    // TODO: Validate new account info. If wrong, return NO, otherwise return YES;
    return YES;
}

- (BOOL)createAccount
{
    if(![self validateInput])
    {
        return NO;
    }
    
    // TODO: Create account
    
    return YES;
}

- (BOOL)saveAccount
{
    if(![self validateInput])
    {
        return NO;
    }
    
    // TODO: Save account changes
    
    return YES;
}

#pragma mark - Navigation

- (IBAction)setAccountImage:(id)sender
{
    [[[UIAlertView alloc] initWithTitle:@"TODO"
                                message:@"Navigate to image picker controller, or set up segue to custom picker."
                               delegate:nil
                      cancelButtonTitle:@"OK"
                      otherButtonTitles:nil] show];
}

- (IBAction)termsOfService:(id)sender
{
    [[[UIAlertView alloc] initWithTitle:@"TODO"
                                message:@"Create Terms of Service Screen"
                               delegate:nil
                      cancelButtonTitle:@"OK"
                      otherButtonTitles:nil] show];
}

- (IBAction)privacyPolicy:(id)sender
{
    [[[UIAlertView alloc] initWithTitle:@"TODO"
                                message:@"Create Privacy Policy"
                               delegate:nil
                      cancelButtonTitle:@"OK"
                      otherButtonTitles:nil] show];
}

- (IBAction)saveAndClose:(id)sender
{
    if(![self saveAccount]) return;
    
    [self close:sender];
}

- (IBAction)close:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

@end
