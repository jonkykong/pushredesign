//
//  pushFirstRunVC].m
//  Push
//
//  Created by Jon Kent on 2/6/15.
//  Copyright (c) 2015 Jon Kent. All rights reserved.
//

#import "pushFirstRunVC.h"
#import "pushKit.h"

@interface pushFirstRunVC ()

@property (nonatomic, weak) IBOutlet UIButton *signUpButton;
@property (nonatomic, weak) IBOutlet UIButton *logInButton;

@end

@implementation pushFirstRunVC

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self.navigationController setNavigationBarHidden:YES animated:YES];
}

@end
