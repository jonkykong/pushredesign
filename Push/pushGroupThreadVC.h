//
//  pushGroupThreadVC.h
//  Push
//
//  Created by Jon Kent on 2/12/15.
//  Copyright (c) 2015 Jon Kent. All rights reserved.
//

#import <UIKit/UIKit.h>

@class pushGroupThreadVC;

@protocol pushGroupThreadVCDelegate <NSObject>

- (void)pushGroupThreadVC:(pushGroupThreadVC *)pushGroupVC didFinishPickingFriends:(NSArray *)friends;
- (void)pushGroupThreadVCDidCancel:(pushGroupThreadVC *)pushGroupVC;

@end

@interface pushGroupThreadVC : UITableViewController

@property (nonatomic, strong) NSString *buttonTitle;
@property (nonatomic, strong) NSString *viewSubtitle;
@property (nonatomic, strong) id<pushGroupThreadVCDelegate> delegate;

@end
