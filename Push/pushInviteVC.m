//
//  pushInviteVC.m
//  Push
//
//  Created by Jon Kent on 2/12/15.
//  Copyright (c) 2015 Jon Kent. All rights reserved.
//

#import "pushInviteVC.h"
#import "pushImageCell.h"
#import <AddressBook/AddressBook.h>

@implementation pushContactSection

@end

@interface pushInviteVC ()

@property (nonatomic, strong) NSArray *suggested;
@property (nonatomic, strong) NSArray *contacts;
@property (nonatomic, strong) NSArray *contactsInSections;
@property (nonatomic, weak) IBOutlet UIButton *sendInvitesButton;
@property (nonatomic, strong) IBOutlet UIView *sendInvitesView;

@end

@implementation pushInviteVC

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    // TODO: populate arrays with real data
    self.suggested = [NSMutableArray arrayWithObjects:@"1", nil];
    [self loadAddressBook];
    [self loadContactsInSections];
    
    self.sendInvitesView = self.tableView.tableFooterView;
    self.tableView.tableFooterView = nil;
    [self.parentViewController.view addSubview:self.sendInvitesView];
    
    self.sendInvitesButton.layer.cornerRadius = 3;
    self.sendInvitesButton.backgroundColor = self.view.tintColor;
    
    CGRect frame = self.sendInvitesView.frame;
    frame.size.height = 60;
    frame.origin.y = self.parentViewController.view.frame.size.height - frame.size.height;
    self.sendInvitesView.frame = frame;
    [self.tableView setContentInset:UIEdgeInsetsMake(self.tableView.contentInset.top,
                                                    self.tableView.contentInset.left,
                                                    frame.size.height,
                                                    self.tableView.contentInset.right)];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    // Return the number of sections.
    if(self.suggested.count > 0) return self.contactsInSections.count + 1;
    
    return self.contactsInSections.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    // Return the number of rows in the section.
    if(section == 0 && self.suggested.count > 0)
    {
        return self.suggested.count;
    }
    
    if(self.suggested.count == 0)
    {
        return ((pushContactSection *)self.contactsInSections[section]).contacts.count;
    }
    
    return ((pushContactSection *)self.contactsInSections[section - 1]).contacts.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    pushImageCell *cell;
    if(indexPath.section == 0 && self.suggested.count > 0)
    {
        cell = [tableView dequeueReusableCellWithIdentifier:
                [pushImageCell cellIdentifierForImageCount:1
                                               hasSubtitle:YES]
                                               forIndexPath:indexPath];
    }
    else
    {
        cell = [tableView dequeueReusableCellWithIdentifier:
                [pushImageCell cellIdentifierForImageCount:1
                                               hasSubtitle:NO]
                                               forIndexPath:indexPath];
    }
    
    cell.tintColor = self.view.tintColor;
    
    // TODO: Configure the cell...
    if(indexPath.section == 0 && self.suggested.count > 0)
    {
        UIImage *image = [UIImage imageNamed:@"headshot.jpg"];
        
        [cell setTitle:@"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas non malesuada tellus. Nam et massa ac dui porta molestie sit amet ac neque." subTitle:@"@username" images:@[image]];
    }
    else
    {
        NSDictionary *user;
        
        if(self.suggested.count == 0)
        {
            user = ((pushContactSection *)self.contactsInSections[indexPath.section]).contacts[indexPath.row];
        }
        else
        {
            user = ((pushContactSection *)self.contactsInSections[indexPath.section - 1]).contacts[indexPath.row];
        }
        
        // don't pass an image to instead use checkmarks for selected items
        [cell setTitle:user[@"name"] subTitle:nil images:nil];
    }
    
    return cell;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    if(section == 0 && self.suggested.count > 0)
    {
        return @"Suggested:";
    }

    if(self.suggested.count == 0)
    {
        return ((pushContactSection *)self.contactsInSections[section]).sectionTitle;
    }
    
    return ((pushContactSection *)self.contactsInSections[section - 1]).sectionTitle;
}

- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return [pushImageCell cellHeight];
}

- (NSArray *)sectionIndexTitlesForTableView:(UITableView *)tableView
{
    NSString *titles = @"ABCDEFGHIJKLMNOPQRSTUVWXYZ#";
    NSMutableArray *titlesArray = [NSMutableArray array];
    
    for(NSUInteger position = 0; position < titles.length; position++)
    {
        [titlesArray addObject:[NSString stringWithFormat: @"%C", [titles characterAtIndex:position]]];
    }
    
    return titlesArray;
}

- (NSIndexPath *)tableView:(UITableView *)tableView willSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(indexPath.section == 0)
    {
        UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
        if(!cell.backgroundView)
        {
            UIView *view = [[UIView alloc] initWithFrame:cell.frame];
            view.backgroundColor = self.view.tintColor;
            cell.selectedBackgroundView = view;
        }
    }
    
    return indexPath;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(indexPath.section == 0)
    {
        // TODO: add user to friends here
        
        UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
        [cell setSelected:NO animated:YES];
    }
}

#pragma mark - Navigation

- (IBAction)close:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)sendInvites:(id)sender
{
    NSArray *indexPaths = [self.tableView indexPathsForSelectedRows];
    
    NSMutableArray *inviteArray = [NSMutableArray arrayWithCapacity:indexPaths.count];
    for(NSIndexPath *indexPath in indexPaths)
    {
        [inviteArray addObject:self.contacts[indexPath.row]];
    }
    
    // TODO: send invites here
    
    [self close:sender];
}

- (void)loadAddressBook{
    self.contacts=[[NSMutableArray alloc] init];
    ABAddressBookRef m_addressbook = ABAddressBookCreateWithOptions(NULL, NULL);
    
    if (!m_addressbook) {
        NSLog(@"opening address book");
    }
    
    NSMutableArray *arr = [NSMutableArray new];
    CFArrayRef allPeople = ABAddressBookCopyArrayOfAllPeople(m_addressbook);
    CFIndex nPeople = ABAddressBookGetPersonCount(m_addressbook);
    for (CFIndex i = 0; i < nPeople; i++) {
        ABRecordRef person = (ABRecordRef)CFArrayGetValueAtIndex(allPeople, i);
        
        NSMutableDictionary *dict = [NSMutableDictionary new];
        
        dict[@"name"]=@"";
        NSString *name = (__bridge_transfer NSString *)ABRecordCopyCompositeName(person);
        if([name length] > 0){
            dict[@"name"]=name;
            dict[@"id"]=[NSString stringWithFormat:@"%d",ABRecordGetRecordID(person)];
            //NSLog(@"the id we put in:%@",dict[@"id"]);
            ABMultiValueRef emailsRef = ABRecordCopyValue(person, kABPersonEmailProperty);
            NSArray* emails = (__bridge NSArray*)ABMultiValueCopyArrayOfAllValues(emailsRef);
            CFRelease(emailsRef);
            
            ABMultiValueRef phoneNumberProp = ABRecordCopyValue(person, kABPersonPhoneProperty);
            NSArray* phoneNumbers = (__bridge NSArray*)ABMultiValueCopyArrayOfAllValues(phoneNumberProp);
            CFRelease(phoneNumberProp);
            
            if(phoneNumbers != nil)
                dict[@"phones"]=phoneNumbers;
            if(emails != nil)
                dict[@"emails"]=emails;
            
            [arr addObject:dict];
        }
    }
    CFRelease(allPeople);
    CFRelease(m_addressbook);
    if([arr count] > 0){
        NSSortDescriptor *sort = [NSSortDescriptor sortDescriptorWithKey:@"name" ascending:YES];
        NSArray *sortedArray=[arr sortedArrayUsingDescriptors:[NSArray arrayWithObject:sort]];
        self.contacts = sortedArray;
        [self.tableView reloadData];
    }
}

- (void)loadContactsInSections
{
    NSMutableArray *sections = [NSMutableArray array];
    NSMutableArray *sectionContacts = [NSMutableArray array];
    pushContactSection *section;
    
    NSCharacterSet *letters = [NSCharacterSet lowercaseLetterCharacterSet];
    NSCharacterSet *numbers = [NSCharacterSet decimalDigitCharacterSet];
    
    for(NSDictionary *dict in self.contacts)
    {
        NSString *prefix;
        
        prefix = dict[@"name"];
        prefix = [prefix substringToIndex:1].lowercaseString;
        
        NSRange range = [prefix rangeOfCharacterFromSet:numbers];
        
        // contains a number...
        if (range.location != NSNotFound)
        {
            prefix = @"A";
        }
        else
        {
            range = [prefix rangeOfCharacterFromSet:letters];
            
            // contains a letter...
            if(range.location != NSNotFound)
            {
                prefix = prefix.uppercaseString;
            }
            else // contains some misc. character...
            {
                prefix = @"#";
            }
        }
        
        // If there was a section being built or the prefix of this contact is different than the prefix from the latest section
        if(!section || ![prefix isEqualToString:((pushContactSection *)sections.lastObject).sectionTitle])
        {
            if(section)
            {
                section.contacts = sectionContacts;
            }
            
            section = [[pushContactSection alloc] init];
            section.sectionTitle = prefix;
            [sections addObject:section];
            
            sectionContacts = [NSMutableArray array];
        }
        
        [sectionContacts addObject:dict];
    }
    
    section.contacts = sectionContacts;
    
    self.contactsInSections = sections;
}

@end
