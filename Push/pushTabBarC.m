//
//  pushTabBarC.m
//  Push
//
//  Created by Jon Kent on 2/9/15.
//  Copyright (c) 2015 Jon Kent. All rights reserved.
//

#import "pushTabBarC.h"
#import "pushFriendsVC.h"
#import "pushGroupThreadVC.h"
#import "pushTalksVC.h"

@interface pushTabBarC () <UITabBarControllerDelegate, pushGroupThreadVCDelegate>

@property (nonatomic, strong) IBOutlet UIBarButtonItem *addButtonFriends;
@property (nonatomic, strong) IBOutlet UIBarButtonItem *addButtonGroup;

@end

@implementation pushTabBarC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    for(UITabBarItem *item in self.tabBar.items)
    {
        item.selectedImage = [item.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    }
    
    self.delegate = self;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self.navigationController setNavigationBarHidden:NO animated:YES];
    
    UITableViewController *tableViewController = (id)self.selectedViewController;
    [self adjustContentInsetForTableView:tableViewController.tableView];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    UINavigationController *navigationController = [segue destinationViewController];
    UIViewController *viewController = navigationController.topViewController;
    if([viewController isKindOfClass:[pushGroupThreadVC class]])
    {
        pushGroupThreadVC *pushGroupVC = (id)viewController;

        if([self.selectedViewController isKindOfClass:[pushTalksVC class]])
        {
            pushGroupVC.buttonTitle = @"PUSH-to-talk";
            pushGroupVC.viewSubtitle = @"Choose friends to Push-to-Talk";
        }
        else
        {
            pushGroupVC.buttonTitle = @"Start Vext";
            pushGroupVC.viewSubtitle = @"Choose friends to Vext";
            pushGroupVC.tableView.backgroundView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"vextBack.jpg"]];
            [pushGroupVC.navigationController.navigationBar setBarStyle:UIBarStyleBlack];
        }
        
        pushGroupVC.delegate = self;
    }
}

#pragma mark - UITabBarControllerDelegate

-(void)tabBarController:(UITabBarController *)tabBarController didSelectViewController:(UIViewController *)viewController
{
    UITableViewController *tableViewController = (id)viewController;
    [self adjustContentInsetForTableView:tableViewController.tableView];
    
    if([tableViewController isKindOfClass:[pushFriendsVC class]])
    {
        [self.navigationItem setRightBarButtonItem:self.addButtonFriends animated:NO];
    }
    else
    {
        [self.navigationItem setRightBarButtonItem:self.addButtonGroup animated:NO];
    }
}

- (void)adjustContentInsetForTableView:(UITableView *)tableView
{
    [tableView setContentInset:UIEdgeInsetsMake(tableView.contentInset.top,
                                                tableView.contentInset.left,
                                                self.tabBar.frame.size.height,
                                                tableView.contentInset.right)];
    [tableView setScrollIndicatorInsets:tableView.contentInset];
}

#pragma mark - pushGroupThreadVCDelegate

- (void)pushGroupThreadVC:(pushGroupThreadVC *)pushGroupVC didFinishPickingFriends:(NSArray *)friends
{
    UITableViewController *tableViewController = (id)self.selectedViewController;
    if([tableViewController isKindOfClass:[pushTalksVC class]])
    {
        // create thread for push to talk here
    }
    else
    {
        // create thread for vext here
    }
}

- (void)pushGroupThreadVCDidCancel:(pushGroupThreadVC *)pushGroupVC
{
    
}

@end
