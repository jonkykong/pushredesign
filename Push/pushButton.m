//
//  pushButton.m
//  Push
//
//  Created by Jon Kent on 2/10/15.
//  Copyright (c) 2015 Jon Kent. All rights reserved.
//

#import "pushButton.h"

@interface pushButton ()

@property (nonatomic, strong) UIColor *originalColor;

@end

@implementation pushButton

- (instancetype)initWithCoder:(NSCoder *)coder
{
    self = [super initWithCoder:coder];
    if (self) {
        self.layer.borderWidth = 1;
        self.layer.cornerRadius = 3;
        self.originalColor = self.backgroundColor;
    }
    return self;
}

- (void)setHighlighted:(BOOL)highlighted
{
    if(highlighted) {
        self.backgroundColor = self.tintColor;
    } else {
        self.backgroundColor = self.originalColor;
    }
    [super setHighlighted:highlighted];
}

- (void)setSelected:(BOOL)selected
{
    if(selected) {
        self.backgroundColor = self.superview.tintColor;
    } else {
        self.backgroundColor = self.originalColor;
    }
    [super setSelected:selected];
}

- (void)layoutSubviews
{
    self.layer.borderColor = self.tintColor.CGColor;
    
    [super layoutSubviews];
}

@end
