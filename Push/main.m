//
//  main.m
//  Push
//
//  Created by Jon Kent on 2/3/15.
//  Copyright (c) 2015 Jon Kent. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "pushAppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([pushAppDelegate class]));
    }
}
