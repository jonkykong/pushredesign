//
//  pushContactCell.m
//  Push
//
//  Created by Jon Kent on 2/9/15.
//  Copyright (c) 2015 Jon Kent. All rights reserved.
//

#import "pushImageCell.h"

@interface pushImageCell ()

@property (nonatomic, weak) IBOutlet UIImageView *image1ImageView;
@property (nonatomic, weak) IBOutlet UIImageView *image2ImageView;
@property (nonatomic, weak) IBOutlet UIImageView *image3ImageView;
@property (nonatomic, weak) IBOutlet UIImageView *image4ImageView;
@property (nonatomic, weak) IBOutlet UIImageView *checkmarkImageView;
@property (nonatomic, weak) IBOutlet UILabel *extraImagesLabel;

@end

static UIImage *uncheckedImage;
static UIImage *checkedImage;

@implementation pushImageCell

- (void)setTitle:(NSString *)title subTitle:(NSString *)subTitle images:(NSArray *)imageArray
{
    self.titleLabel.text = title;
    self.subtitleLabel.text = subTitle;
    
    if(imageArray.count >= 1) self.image1ImageView.image = imageArray[0];
    if(imageArray.count >= 2) self.image2ImageView.image = imageArray[1];
    if(imageArray.count >= 3) self.image3ImageView.image = imageArray[2];
    if(imageArray.count == 4) self.image4ImageView.image = imageArray[3];
    
    if(imageArray.count > 4)
    {
        NSUInteger diff = imageArray.count - 3;
        if(diff > 99) diff = 99;
        self.extraImagesLabel.text = [NSString stringWithFormat:@"+%lu", diff];
    }
    else
    {
        [self.extraImagesLabel setHidden:YES];
    }
    
    self.image1ImageView.layer.cornerRadius = self.image1ImageView.frame.size.width / 2.0;
    self.image2ImageView.layer.cornerRadius = self.image2ImageView.frame.size.width / 2.0;
    self.image3ImageView.layer.cornerRadius = self.image3ImageView.frame.size.width / 2.0;
    self.image4ImageView.layer.cornerRadius = self.image4ImageView.frame.size.width / 2.0;
    
    self.image4ImageView.backgroundColor = self.tintColor;
    
    // Hack to fix Xcode table cell sizing issues: http://stackoverflow.com/questions/26230247/auto-resizing-issue-in-uitableview-in-xcode-6
    CGRect frame = self.contentView.frame;
    frame.size.width = [UIScreen mainScreen].bounds.size.width;
    self.contentView.frame = frame;
    
    if(self.subtitleLabel)
    {
        CGFloat fromRight = 8;
        
        // if we have a checkmark and an image, make room for the checkmark on the right side
        if(self.image1ImageView && self.checkmarkImageView)
        {
            frame = self.checkmarkImageView.frame;
            frame.origin.x = [UIScreen mainScreen].bounds.size.width - self.checkmarkImageView.frame.size.width - fromRight;
            self.checkmarkImageView.frame = frame;
            self.checkmarkImageView.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin;
            
            fromRight = self.checkmarkImageView.frame.size.width + fromRight * 2;
        }
        
        frame = self.titleLabel.frame;
        frame.size.width = [UIScreen mainScreen].bounds.size.width - self.titleLabel.frame.origin.x - fromRight;
        self.titleLabel.frame = frame;
        
        frame = self.subtitleLabel.frame;
        frame.size.width = [UIScreen mainScreen].bounds.size.width - self.subtitleLabel.frame.origin.x - fromRight;
        self.subtitleLabel.frame = frame;
        
        self.titleLabel.autoresizingMask = UIViewAutoresizingFlexibleWidth;
        self.subtitleLabel.autoresizingMask = UIViewAutoresizingFlexibleWidth;
    }
    else
    {
        frame = self.titleLabel.frame;
        frame.size.width = [UIScreen mainScreen].bounds.size.width - self.titleLabel.frame.origin.x - 8;
        self.titleLabel.frame = frame;
        
        self.titleLabel.autoresizingMask = UIViewAutoresizingFlexibleWidth |
                                            UIViewAutoresizingFlexibleTopMargin |
                                            UIViewAutoresizingFlexibleBottomMargin;
    }
}

+ (NSString *)cellIdentifierForImageCount:(NSUInteger)count hasSubtitle:(BOOL)subtitle
{
    if(subtitle) return @"ImageCellSubtitle";
    
    if(count > 4) count = 4; // we only support layouts of up to 8 images
    
    return [NSString stringWithFormat:@"ImageCell%lu", (unsigned long)count];
}

+ (CGFloat)cellHeight
{
    return 66.0;
}

- (void)setHighlighted:(BOOL)highlighted animated:(BOOL)animated
{
    [super setHighlighted:highlighted animated:animated];
    
    self.image4ImageView.backgroundColor = self.tintColor;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    [self setHighlighted:selected animated:animated];
    
    if(self.checkmarkImageView)
    {
        if(!checkedImage)
        {
            checkedImage = [UIImage imageNamed:@"checkedLight.png"];
        }
        
        if(!uncheckedImage)
        {
            uncheckedImage = [UIImage imageNamed:@"unChecked.png"];
        }
        
        if(selected)
        {
            self.checkmarkImageView.image = checkedImage;
        }
        else
        {
            self.checkmarkImageView.image = uncheckedImage;
        }
    }
}

@end
