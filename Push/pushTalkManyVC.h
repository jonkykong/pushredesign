//
//  pushTalkManyVC.h
//  Push
//
//  Created by Jon Kent on 2/11/15.
//  Copyright (c) 2015 Jon Kent. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface pushTalkManyVC : UITableViewController

@property (nonatomic, strong) NSArray *recipients;

@end
