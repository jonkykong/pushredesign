//
//  pushPermissionsVC.h
//  Push
//
//  Created by Jon Kent on 2/7/15.
//  Copyright (c) 2015 Jon Kent. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef enum {
    PushPermissionsTypeMicrophone,
    PushPermissionsTypeContacts,
    PushPermissionsTypeNotifications
} PushPermissionsType;

@interface pushPermissionsVC : UITableViewController

@property (nonatomic, assign) PushPermissionsType permissionType;

@end
