//
//  pushFriendOptionsVC.m
//  Push
//
//  Created by Jon Kent on 2/10/15.
//  Copyright (c) 2015 Jon Kent. All rights reserved.
//

#import "pushFriendOptionsVC.h"
#import "pushVextVC.h"

@interface pushFriendOptionsVC ()

@property (nonatomic, weak) IBOutlet UIImageView *friendImageView;
@property (nonatomic, weak) IBOutlet UILabel *nameLabel;
@property (nonatomic, weak) IBOutlet UILabel *usernameLabel;
@property (nonatomic, weak) IBOutlet UIButton *pushToTalkButton;
@property (nonatomic, weak) IBOutlet UIImageView *icon1ImageView;
@property (nonatomic, weak) IBOutlet UIImageView *icon2ImageView;
@property (nonatomic, weak) IBOutlet UIImageView *icon3ImageView;

@end

@implementation pushFriendOptionsVC

- (void)viewDidLoad
{
    CGSize statusBarSize = [[UIApplication sharedApplication] statusBarFrame].size;
    CGFloat statusBarHeight = fminf(statusBarSize.width, statusBarSize.height); //accomodate for screen rotation, if necessary
    CGFloat navigationHeight = statusBarHeight + self.navigationController.navigationBar.frame.size.height;
    
    CGRect frame = self.tableView.tableHeaderView.frame;
    frame.size.height = self.tableView.frame.size.height - navigationHeight - 44 * 3;
    self.tableView.tableHeaderView.frame = frame;
    
    frame = self.friendImageView.frame;
    frame.size.width = frame.size.height;
    frame.origin.x = ((self.tableView.tableHeaderView.frame.size.width - frame.size.width) / 2.0);
    self.friendImageView.frame = frame;
    self.friendImageView.layer.cornerRadius = self.friendImageView.frame.size.height / 2.0;
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    UIViewController *viewController = [segue destinationViewController];
    if([viewController isKindOfClass:[pushVextVC class]])
    {
        pushVextVC *pushVextVC = (id)viewController;
        pushVextVC.recipients = @[self.contact];
    }
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    // Hack to fix Xcode table cell sizing issues: http://stackoverflow.com/questions/26230247/auto-resizing-issue-in-uitableview-in-xcode-6
    CGRect frame = self.icon1ImageView.frame;
    frame.origin.x = [UIScreen mainScreen].bounds.size.width - self.icon1ImageView.frame.size.width - 30;
    self.icon1ImageView.frame = frame;
    self.icon2ImageView.frame = frame;
    self.icon3ImageView.frame = frame;
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    // Hack to fix Xcode table cell sizing issues: http://stackoverflow.com/questions/26230247/auto-resizing-issue-in-uitableview-in-xcode-6
    self.icon1ImageView.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin;
    self.icon2ImageView.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin;
    self.icon3ImageView.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin;
}

@end
