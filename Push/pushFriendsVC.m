//
//  pushFriendsVC.m
//  Push
//
//  Created by Jon Kent on 2/9/15.
//  Copyright (c) 2015 Jon Kent. All rights reserved.
//

#import "pushFriendsVC.h"
#import "pushImageCell.h"
#import "pushFriendOptionsVC.h"

@interface pushFriendsVC ()

@property (nonatomic, strong) NSArray *contacts;

@end

@implementation pushFriendsVC

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    // TODO: populate arrays with real data
    self.contacts = [NSMutableArray arrayWithObjects:@"1", @"2", @"3", @"4", @"5", @"6", @"7", @"8", @"9", @"10", nil];
    
    if(self.contacts.count > 0)
    {
        self.tableView.tableHeaderView = nil; // get rid of welcome message
        self.tableView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
    }
    else
    {
        self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    }
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    UIViewController *viewController = [segue destinationViewController];
    if([viewController isKindOfClass:[pushFriendOptionsVC class]])
    {
        pushFriendOptionsVC *pushOptionsVC = (id)viewController;
        NSIndexPath *indexPath = [self.tableView indexPathForCell:sender];
        pushOptionsVC.contact = self.contacts[indexPath.row];
    }
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {

    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {

    // Return the number of rows in the section.
    return self.contacts.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    pushImageCell *cell = [tableView dequeueReusableCellWithIdentifier:
                             [pushImageCell cellIdentifierForImageCount:1
                                                            hasSubtitle:YES]
                                                            forIndexPath:indexPath];
    
    cell.tintColor = self.view.tintColor;
    
    // TODO: Configure the cell...
    UIImage *image = [UIImage imageNamed:@"headshot.jpg"];
    [cell setTitle:@"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas non malesuada tellus. Nam et massa ac dui porta molestie sit amet ac neque." subTitle:@"@username" images:@[image]];
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return [pushImageCell cellHeight];
}

@end
