//#import "pushKit.h"
//
//#pragma mark - Color
//@implementation UIColor (push)
//
//+ (instancetype)colorFromHexString:(NSString *)hexString
//{
//    unsigned rgbValue = 0;
//    hexString = [hexString stringByReplacingOccurrencesOfString:@"#" withString:@""];
//    NSScanner *scanner = [NSScanner scannerWithString:hexString];
//    [scanner scanHexInt:&rgbValue];
//    
//    return [[self class] colorWithRed:((rgbValue & 0xFF0000) >> 16)/255.0 green:((rgbValue & 0xFF00) >> 8)/255.0 blue:(rgbValue & 0xFF)/255.0 alpha:1.0];
//}
//
//+ (instancetype)keepsakePink
//{
//    return [UIColor colorWithRed:242.0f/255.0f green:19.0f/255.0f blue:106.0f/255.0f alpha:1.0f];
//}
//
//+ (instancetype)keepsakeNavy
//{
//    return [UIColor colorWithRed:1.0/255.0 green:0.0/255.0 blue:60.0/255.0 alpha:1.0f];
//}
//
//@end
//
//#pragma mark - UIView
//
//@implementation UIView (push)
//
//- (instancetype)blur
//{
//    UIVisualEffect *blurEffect;
//    blurEffect = [UIBlurEffect effectWithStyle:UIBlurEffectStyleLight];
//    
//    UIVisualEffectView *visualEffectView;
//    visualEffectView = [[UIVisualEffectView alloc] initWithEffect:blurEffect];
//    
//    visualEffectView.frame = self.bounds;
//    [self addSubview:visualEffectView];
//    [self sendSubviewToBack:visualEffectView];
//}
//
//- (instancetype)vibrantBlur
//{
//    // Vibrancy effect
//    UIVibrancyEffect *vibrancyEffect = [UIVibrancyEffect effectForBlurEffect:blurEffect];
//    UIVisualEffectView *vibrancyEffectView = [[UIVisualEffectView alloc] initWithEffect:vibrancyEffect];
//    [vibrancyEffectView setFrame:self.view.bounds];
//}
//
//@end